<?php

namespace Idoom\Vitamin;

use Illuminate\Database\Eloquent\Model;

class VitaminName extends Model
{
    //
    protected $fillable = ['name','user_id'];
    protected $table = "vitamin_names";
    
    public function vitamins(){
        return $this->hasMany('Idoom\Vitamin\Vitamin','vitamin_name_id','id');
    }
}
