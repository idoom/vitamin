<form id="vitamin_create" action="{{route('vitamins.import.save')}}" method="POST">
    @csrf
    <div class="modal-header">
        <h4></h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    </div>
    <div class="modal-body">
        <legend class="text-center">Create Vitamin</legend>
        <div class="alert alert-danger print-error-msg" style="display:none"></div>
        <fieldset>
            <div class="form-group row m-b-15">
                <label class="col-md-3 col-form-label  text-right">Vitamin Name</label>
                <div class="col-md-7">
                    <input type="text" class="form-control" name="name" placeholder="nama vitamin" required>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div id="buatngedrop" class="dropzone"></div>
        </fieldset>
    </div>
    <div class="modal-footer">
        <button id="modal-close" class="btn btn-white" data-dismiss="modal">Close</button>
        <button id="submit" type="submit" class="btn btn-primary"> Simpan <i class="fas fa-lg fa-fw fa-save"></i> </button>
    </div>
</form>

<script>
Dropzone.autoDiscover = false;
var fullpaths = []
var mDropZone = new Dropzone("div#buatngedrop", { 
        url: "{{route('vitamins.upload')}}",
        acceptedFiles:".txt",
        timeout:600000,
        uploadMultiple:false,
        chunking:true,
        forceChunking:true,
        chunkSize:500000,
        parallelUploads:1,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        init: function() {
            this.on('addedfile',function(file){
                $('button#modal-close').attr('disabled',true);
                $('button.close').attr('disabled',true);
                $('button[type=submit]').attr('disabled',true);
                $('button[type=submit] > i.fas').removeClass('fa-save').addClass('fa-spinner fa-pulse');
            });
            this.on("sending", function(file, xhr, formData) {
                var dropzoneOnLoad = xhr.onload;
                xhr.onload = function (e) {
                    dropzoneOnLoad(e)
                    var uploadResponse = JSON.parse(xhr.responseText)
                    if (typeof uploadResponse.name === 'string') {
                        fullpaths.push(uploadResponse.fullpath);
                        console.log(fullpaths);
                        //$list.append('<li>Uploaded: ' + uploadResponse.path + uploadResponse.name + '</li>')
                    }
                }
            });
          }
    });
    mDropZone.on("error", function(file,errorMessage) {
        mDropZone.removeFile(file);
        console.log(errorMessage);
        swal({
            title: 'Error',
            text: errorMessage.message,
            icon: 'error',
            timer: 1800,
            buttons: {
                confirm: {
                    text: 'Ok',
                    value: true,
                    visible: true,
                    className: 'btn btn-success',
                    closeModal: true
                }
            }
        });
    });
    mDropZone.on("success", function(file,resp) {
        $('button#modal-close').attr('disabled',false);
        $('button.close').attr('disabled',false);
        $('button[type=submit]').attr('disabled',false);
        $('button[type=submit] > i.fas').removeClass('fa-spinner').removeClass('fa-pulse').addClass('fa-save');
    });
$(document).ready(function(e){
    $('form#vitamin_create').submit(function(e){
        e.preventDefault();
        $('button#modal-close').attr('disabled',true);
        $('button.close').attr('disabled',true);
        $('button[type=submit]').attr('disabled',true);
        $('button[type=submit] > i.fas').removeClass('fa-save').addClass('fa-spinner fa-pulse');
        var url = $(this).attr('action');
        var data = $(this).serializeArray();
        data.push({name:'filenames',value:fullpaths});
        $.post(url,data,function(response){
            if(response.status !== 'sukses'){
                printErrorMsg(response.error);
                swal({
                    title: 'Error',
                    text: response.message,
                    icon: 'error',
                    timer: 1800,
                    buttons: {
                        confirm: {
                            text: 'Ok',
                            value: true,
                            visible: true,
                            className: 'btn btn-success',
                            closeModal: true
                        }
                    }
                });
            }else{
                swal({
                    title: 'Sukses',
                    text: response.message,
                    icon: 'success',
                    timer: 1800,
                    buttons: {
                        confirm: {
                            text: 'Ok',
                            value: true,
                            visible: true,
                            className: 'btn btn-success',
                            closeModal: true
                        }
                    }
                });
                //reload datatables
                vitaminname_table.draw();
                $('#modal-message').modal('hide');
            }
        })
    });
    function printErrorMsg (msg) {

        $(".print-error-msg").find("ul").html('');

        $(".print-error-msg").css('display','block');

        $.each( msg, function( key, value ) {

            $(".print-error-msg").find("ul").append('<li>'+value+'</li>');

        });

    }
});
</script>