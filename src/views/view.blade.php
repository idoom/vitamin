<div class="modal-header">
    <h4 class="modal-title">View Vitamin {{ $vitaminname->name }}</h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
</div>
<div class="modal-body">
<table id="vitamins_table" class="table table-striped table-bordered">
    <thead>
        <tr>
            <th width="1%">#</th>
            <th class="text-nowrap">name</th>
        </tr>
    </thead>
    <tbody>
        
    </tbody>
</table>
</div>
<div class="modal-footer">
    <button id="modal-close" class="btn btn-white" data-dismiss="modal">Close</button>
</div>
<script>
$(document).ready(function(e){
    vitamins_table = $('#vitamins_table').DataTable({
        processing: true,
        bAutoWidth: false,
        serverSide: true,
        lengthMenu: [50, 100, 200, 500],
        fixedHeader: {
            header: true,
            headerOffset: $('#header').height()
        },
        responsive: true,
        ajax: {
            url: '{{ route("vitamins.getall",$vitaminname->id) }}',
            type: "POST"
        },
        columns: [
            { data: 'id', name: 'id' },
            { data: 'vitamin', name: 'vitamin' }
        ]
    });
});
</script>