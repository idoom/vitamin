@extends('layouts.default')
@section('title', 'Vitamin ')
@push('css')
	<link href="/assets/plugins/datatables/css/dataTables.bootstrap4.css" rel="stylesheet" />
	<link href="/assets/plugins/datatables/css/fixedHeader/fixedHeader.bootstrap4.min.css" rel="stylesheet" />
	<link href="/assets/plugins/datatables/css/responsive/responsive.bootstrap4.css" rel="stylesheet" />
@endpush
@section('content')
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
		<li class="breadcrumb-item active"><span>Vitamin</span></li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Vitamin</h1>
	<!-- end page-header -->
	
	<!-- begin row -->
	<div class="row">
		<!-- begin col-10 -->
		<div class="col-lg-12">
			<!-- begin panel -->
			<div class="panel panel-inverse">
				<!-- begin panel-heading -->
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
					</div>
					<h4 class="panel-title">Vitamin</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body">
					<table id="vitaminname_table" class="table table-striped table-bordered">
						<thead>
							<tr>
								<th width="1%">#</th>
								<th class="text-nowrap">name</th>
								<th width="10%">Action</th>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
					</table>
                    <center><button id="vitamins_new" href="#modal-message" class="btn btn-primary btn-lg" data-href="{{ route('vitamins.create') }}" data-toggle="modal">Create Vitamin</button> <button id="vitamins_new" href="#modal-message" class="btn btn-primary btn-lg" data-href="{{ route('vitamins.import') }}" data-toggle="modal">Import Vitamin</button></center>
				</div>
				<!-- end panel-body -->
			</div>
			<!-- end panel -->
		</div>
		<!-- end col-10 -->
	</div>
	<!-- end row -->
    <div class="modal modal-message fade" id="modal-message">
        <div class="modal-dialog">
            <div class="modal-content">
                
            </div>
        </div>
    </div>
@endsection

@push('inc-css')
    <link href="/assets/plugins/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />
    <link href="/assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
    <link href="/assets/plugins/dropzone/dist/min/dropzone.min.css" rel="stylesheet" />
@endpush
@push('inc-scripts')
    <script src="/assets/plugins/datatables/js/jquery.dataTables.js"></script>
	<script src="/assets/plugins/datatables/js/dataTables.bootstrap4.js"></script>
	<script src="/assets/plugins/datatables/js/fixedHeader/dataTables.fixedHeader.js"></script>
	<script src="/assets/plugins/datatables/js/responsive/dataTables.responsive.js"></script>
	<script src="/assets/plugins/datatables/js/responsive/responsive.bootstrap4.js"></script>
    <script src="/assets/plugins/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
    <script src="/assets/plugins/select2/dist/js/select2.min.js"></script>
    <script src="/assets/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="/assets/js/demo/table-manage-fixed-header.demo.js"></script>
    <script src="/assets/plugins/dropzone/dist/min/dropzone.min.js"></script>
@endpush

@push('scripts')
	<script>
		$(document).ready(function() {TableManageFixedHeader.init();
            vitaminname_table = $('#vitaminname_table').DataTable({
                processing: true,
                bAutoWidth: false,
                serverSide: true,
                lengthMenu: [50, 100, 200, 500],
                fixedHeader: {
                    header: true,
                    headerOffset: $('#header').height()
                },
                responsive: true,
                ajax: {
                    url: '{{ route("vitamins.all") }}',
                    type: "POST",
                    data:{
                        action : "get_all_role",
                        //_token : csrf_token
                    }
                },
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'name', name: 'name' },
                    { data: 'action', name: 'action' , orderable: false, searchable: false},
                ],
                drawCallback: function( settings ) {
                    var api = this.api();
                    var data = api.rows( {page:'current'} ).data();
                    for(var i = 0; i < data.length; i++){
                        $("form#f-delete-vitamins-"+data[i].id).unbind();
                        $("form#f-delete-vitamins-"+data[i].id).submit(function(e){
                            e.preventDefault();
                            var url = $(this).attr('action');
                            var data = $(this).serializeArray();
                            swal({
                                title: 'Peringatan!!!',
                                text: 'Anda tidak dapat memperbaiki kesalahan yang disebabkan oleh penghapusan data ini',
                                icon: 'error',
                                buttons: {
                                    cancel: {
                                        text: 'Batal',
                                        value: null,
                                        visible: true,
                                        className: 'btn btn-default',
                                        closeModal: true,
                                    },
                                    confirm: {
                                        text: 'Hapus!!',
                                        value: true,
                                        visible: true,
                                        className: 'btn btn-danger',
                                        closeModal: false
                                    }
                                }
                            }).then((value) => {
                                switch(value){
                                    case true:
                                        $.post(url,data,function(response){
                                            if(response.status !== 'sukses'){
                                                swal({
                                                    title: 'Error',
                                                    text: response.message,
                                                    icon: 'error',
                                                    timer: 1800,
                                                    buttons: {
                                                        confirm: {
                                                            text: 'Ok',
                                                            value: true,
                                                            visible: true,
                                                            className: 'btn btn-success',
                                                            closeModal: true
                                                        }
                                                    }
                                                });
                                                printErrorMsg(response.error);
                                            }else{
                                                swal({
                                                    title: 'Sukses',
                                                    text: response.message,
                                                    icon: 'success',
                                                    timer: 1800,
                                                    buttons: {
                                                        confirm: {
                                                            text: 'Ok',
                                                            value: true,
                                                            visible: true,
                                                            className: 'btn btn-success',
                                                            closeModal: true
                                                        }
                                                    }
                                                });
                                                //reload datatables
                                                vitaminname_table.draw();
                                            }
                                        });
                                        break;
                                    default:
                                        break;
                                }
                            });
                        });
                    }
                }
            });
            $("#modal-message").on("show.bs.modal", function(e) {
                var link = $(e.relatedTarget);
                $(this).find(".modal-content").empty().load(link.data('href'));
            })
		});
	</script>
@endpush