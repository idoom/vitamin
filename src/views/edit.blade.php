<form id="vitamin_create" action="{{route('vitamins.update',$vitaminname->id)}}" method="POST">
    @csrf
    <input name="_method" value="patch" type="hidden">
    <div class="modal-header">
        <h4 class="modal-title">Create Vitamin</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    </div>
    <div class="modal-body">
        <div class="alert alert-danger print-error-msg" style="display:none"></div>
        <fieldset>
            <legend class="m-b-15">Vitamin Name</legend>
            <div class="form-group row m-b-15">
                <label class="col-md-2 col-form-label">Vitamin Name</label>
                <div class="col-md-10">
                    <input type="text" class="form-control" readonly name="name" placeholder="nama vitamin" value="{{ $vitaminname->name }}" required>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group row m-b-15">
                <label class="col-md-2 col-form-label">Vitamins</label>
                <div class="col-md-10">
                    <textarea class="form-control" name="vitamins" placeholder="vitamin gaes" rows=35 required>{{ $vitamins }}</textarea>
                </div>
            </div>
        </fieldset>
    </div>
    <div class="modal-footer">
        <button id="modal-close" class="btn btn-white" data-dismiss="modal">Close</button>
        <button id="submit" type="submit" class="btn btn-primary"> Save <i class="fas fa-lg fa-fw fa-save"></i> </button>
    </div>
</form>

<script>
$(document).ready(function(e){
    $(".multiple-select2").select2({ placeholder: "Select permissions" });
    $('form#vitamin_create').submit(function(e){
        e.preventDefault();
        $('button#modal-close').attr('disabled',true);
        $('button.close').attr('disabled',true);
        $('button[type=submit]').attr('disabled',true);
        $('button[type=submit] > i.fas').removeClass('fa-save').addClass('fa-spinner fa-pulse');
        var url = $(this).attr('action');
        var data = $(this).serializeArray();
        $.post(url,data,function(response){
            if(response.status !== 'sukses'){
                printErrorMsg(response.error);
                swal({
                    title: 'Error',
                    text: response.message,
                    icon: 'error',
                    timer: 1800,
                    buttons: {
                        confirm: {
                            text: 'Ok',
                            value: true,
                            visible: true,
                            className: 'btn btn-success',
                            closeModal: true
                        }
                    }
                });
            }else{
                swal({
                    title: 'Sukses',
                    text: response.message,
                    icon: 'success',
                    timer: 1800,
                    buttons: {
                        confirm: {
                            text: 'Ok',
                            value: true,
                            visible: true,
                            className: 'btn btn-success',
                            closeModal: true
                        }
                    }
                });
                //reload datatables
                vitaminname_table.draw();
                $('#modal-message').modal('hide');
            }
        })
    });
    function printErrorMsg (msg) {

        $(".print-error-msg").find("ul").html('');

        $(".print-error-msg").css('display','block');

        $.each( msg, function( key, value ) {

            $(".print-error-msg").find("ul").append('<li>'+value+'</li>');

        });

    }
});
</script>