<?php

namespace Idoom\Vitamin;

use Illuminate\Database\Eloquent\Model;

class Vitamin extends Model
{
    //
    protected $fillable = ['vitamin','vitamin_name_id'];
    protected $table = "vitamins";
    public function name(){
        return $this->belongsTo('Idoom\Vitamin\VitaminName','id','vitamin_name_id');
    }
}
