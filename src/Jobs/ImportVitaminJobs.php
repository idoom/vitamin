<?php

namespace Idoom\Vitamin\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Monolog\Logger;
use Monolog\Handler\RotatingFileHandler;
use Idoom\Vitamin\Vitamin; 
use Idoom\Vitamin\VitaminName;
use Storage;
class ImportVitaminJobs implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $timeout = 600;
    public $name;
    public $user_id;
    public $filename;
    private $log;
    private $hdr;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user_id,$name,$filename)
    {
        //
        $this->name = $name;
        $this->user_id = $user_id;
        $this->filename = $filename;
        $this->log = new Logger('- Import Vitamin '.$this->name.' '.$this->filename);
        $this->log->pushHandler(new RotatingFileHandler('/tmp/importvitamin.log',2,Logger::INFO));
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->log->info('start');
        sleep(rand(1,5));
        $vn = VitaminName::firstOrNew(['name'=> $this->name],['user_id'=>$this->user_id]);
        if(!$vn->id) {
            $vn->save();
        }
        $this->log->info($vn->name);
        if(!Storage::exists($this->filename)){
            return true;
        }
        $tdatas = Storage::get($this->filename);
        $tdatas = str_replace("\r","",trim($tdatas));
        $datas = explode("\n",$tdatas);
        $datas = collect($datas);
        $chunks = $datas->chunk(10000);
        $tot = count($chunks);
        $a = 1;
        foreach($chunks as $chs){
            $data = [];
            foreach($chs as $ch){
                $data[] = ['vitamin'=>$ch,'vitamin_name_id'=>$vn->id];
            }
            $this->log->info('processing '.$a.'/'.$tot.' data');
            Vitamin::insert($data);
            $a++;
        }
        $this->log->info('done');
        Storage::delete($this->filename);
    }
    
    public function failed($exception)
    {
        
    }
}
