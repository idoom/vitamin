<?php

namespace Idoom\Vitamin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Pion\Laravel\ChunkUpload\Exceptions\UploadMissingFileException;
use Pion\Laravel\ChunkUpload\Handler\AbstractHandler;
use Pion\Laravel\ChunkUpload\Receiver\FileReceiver;
use Illuminate\Http\UploadedFile;

use DataTables;
use Validator;
use Idoom\Vitamin\Vitamin; 
use Idoom\Vitamin\VitaminName;
use Idoom\Vitamin\Jobs\ImportVitaminJobs;
class VitaminController extends Controller
{
    //
    public function __construct(){
        
    }
    
    public function index(){
        
        return view('idoom.vitamins.index');
    }
    
    public function all(Request $request){
        $vitamins = VitaminName::query();
		return DataTables::of($vitamins)
			->editColumn('action',function($item){
            
                $out = '
                <a id="view-vitamins-'.$item->id.'"  data-href="'.route('vitamins.view',$item->id).'" alt="view" title="view" href="#modal-message" class="btn btn-xs btn-circle btn-primary" data-toggle="modal"><i class="fa fa-eye fa-xs"></i></a>
                <a id="edit-vitamins-'.$item->id.'"  data-href="'.route('vitamins.edit',$item->id).'" alt="edit" title="edit" href="#modal-message" class="btn btn-xs btn-circle btn-primary" data-toggle="modal"><i class="fa fa-edit fa-xs"></i></a>
                <a id="delete-vitamins-'.$item->id.'" data-id="'.$item->id.'" alt="delete" onclick="$(\'form#f-delete-vitamins-'.$item->id.'\').submit();" title="delete" href="javascript:;" class="btn btn-xs btn-danger btn-circle"><i class="fa fa-trash fa-xs"></i></a>';
                $out .= '<form id="f-delete-vitamins-'.$item->id.'" method="post" action="'.route('vitamins.delete',$item->id).'">
                    '.csrf_field().'
                    <input type="hidden" name="_method" value="delete" />
                    <input type="hidden" name="id" value="'.$item->id.'" />
                    </form>
                ';
                
                return $out;
			})
			->make(true);
    }
    
    public function create(Request $request){
        return view('idoom.vitamins.create');
    }
    
    public function import(Request $request){
        return view('idoom.vitamins.import');
    }
    public function view(Request $request,$id){
        $vitaminname = VitaminName::find($id);
        return view('idoom.vitamins.view',compact('vitaminname'));
    }
    public function edit(Request $request,$id){
        $vitaminname = VitaminName::find($id);
        $vitamins = $vitaminname->vitamins->implode("vitamin","\n");
        return view('idoom.vitamins.edit',compact('vitaminname','vitamins'));
    }
    public function getall(Request $request,$id){
        $vitamins = VitaminName::find($id)->vitamins();
        return DataTables::of($vitamins)
			->make(true);
    }
    
    public function bulkcreate(Request $request){
        return view('idoom.vitamins.bulkcreate');
    }
    
    public function insert(Request $request){
        $validator = Validator::make($request->all(),[
            'name' => 'required|string|max:255',
            'vitamins' => 'required|string'
        ]);
        if($validator->passes()){
            $validated = $validator->validated();
            $name = $validated['name'];
            $vitamins = str_replace("\r","",$validated['vitamins']);
            $vitamins = explode("\n",$vitamins);
            $ivita = new VitaminName();
            $ivita->name = $name;
            $ivita->user_id = \Auth::user()->id;
            $ivita->save();
            foreach($vitamins as $vita){
                $ivita->vitamins()->save(new Vitamin(['vitamin'=>$vita]));
            }
            return response()->json(['status'=>'sukses','message'=>'Added new records.']);
        }
        return response()->json(['status'=>'error','error'=>$validator->errors()->all()]);
    }
    
    public function update(Request $request){
        $validator = Validator::make($request->all(),[
            'name' => 'required|string|max:255',
            'vitamins' => 'required|string'
        ]);
        if($validator->passes()){
            $validated = $validator->validated();
            $name = $validated['name'];
            $vitamins = str_replace("\r","",$validated['vitamins']);
            $vitamins = collect(explode("\n",$vitamins))->map(function($v,$k){
                return ['vitamin'=>$v];
            })->toArray();
            
            $ivita = VitaminName::firstOrNew(['name'=>$name,'user_id'=>\Auth::user()->id]);
            $ivita->save();
            $ivita->vitamins()->delete();
            $ivita->vitamins()->createMany($vitamins);
            
            return response()->json(['status'=>'sukses','message'=>'Added new records.']);
        }
        return response()->json(['status'=>'error','error'=>$validator->errors()->all()]);
    }
    
    public function upload(FileReceiver $receiver){
        // check if the upload is success, throw exception or return response you need
        if ($receiver->isUploaded() === false) {
            throw new UploadMissingFileException();
        }
        // receive the file
        $save = $receiver->receive();

        // check if the upload has finished (in chunk mode it will send smaller files)
        if ($save->isFinished()) {
            // save the file and return any response you need
            return $this->saveFile($save->getFile());
        }

        // we are in chunk mode, lets send the current progress
        /** @var AbstractHandler $handler */
        $handler = $save->handler();
        return response()->json([
            "done" => $handler->getPercentageDone()
        ]);
    }
    
    public function delete(Request $request,$id){
        $vitaminname = VitaminName::find($id);
        $vitaminname->delete();
        
        return response()->json(['status'=>'sukses','message'=>'vitamin berhasil dihapus']);
    }
    
    public function saveimport(Request $request){
        $validator = Validator::make($request->all(),[
            'name' => 'required|string|max:255',
            'filenames' => 'required|string'
        ]);
        if($validator->passes()){
            $validated = $validator->validated();
            $name = $validated['name'];
            $filenames = explode(',',$validated['filenames']);
            $vqueue = config('vitamin.queue');
            foreach($filenames as $fn){
                ImportVitaminJobs::dispatch($request->user()->id,$name,$fn)->onQueue($vqueue);
            }
            return response()->json(['status'=>'sukses','message'=>'Import Sedang Dijalankan di Background dengan worker:'.$vqueue]);
        }
    }
    
    protected function saveFile(UploadedFile $file)
    {
        $fileName = $this->createFilename($file);
        $mime = str_replace('/', '-', $file->getMimeType());
        
        $filePath = "vitamin/";
        $finalPath = storage_path("app/".$filePath);
        $file->move($finalPath, $fileName);

        return response()->json([
            'path' => $filePath,
            'fullpath' => $filePath.$fileName,
            'name' => $fileName,
            'mime_type' => $mime
        ]);
    }
    
    protected function createFilename(UploadedFile $file)
    {
        $extension = $file->getClientOriginalExtension();
        $filename = str_replace(".".$extension, "", $file->getClientOriginalName()); // Filename without extension
        $filename .= "_" . md5(time()) . "." . $extension;
        return $filename;
    }

}
