<?php

namespace Idoom\Vitamin;

use Illuminate\Support\ServiceProvider;

class VitaminServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->loadRoutesFrom(__DIR__.'/routes.php');
        $this->loadMigrationsFrom(__DIR__.'/migrations');
        $this->loadViewsFrom(__DIR__.'/views', 'vitamins');
        $this->publishes([
            __DIR__.'/views' => base_path('resources/views/idoom/vitamins'),
            __DIR__.'/migrations' => base_path('/database/migrations'),
        ]);
        $this->publishes([
            __DIR__.'/../config/vitamin.php' => config_path('vitamin.php'),
        ], 'config');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->app->make('Idoom\Vitamin\VitaminController');
    }
}
